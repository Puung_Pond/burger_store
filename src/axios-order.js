import Axios from "axios"

const instance = Axios.create({
    baseURL: "https://burger-st.firebaseio.com/"
})

export default instance