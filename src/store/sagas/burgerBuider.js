import Axios from "../../axios-order"
import { put } from "redux-saga/effects"

import * as actions from "../actions/index"

export function* initIngredientsSaga(action) {
    try {
        const response = yield Axios.get("https://burger-st.firebaseio.com/ingredients.json")
        yield put(actions.setIngredients(response.data))
    } catch (error) {
        yield put(actions.fetchIngredientsFailed())
    }
}