import { put } from "redux-saga/effects"
import Axios from "../../axios-order"
import * as actions from "../actions/index"

export function* purchaseBurgerSaga(action) {
    yield put(actions.purchaseBurgerStart)
    try {
        const response = yield Axios.post(`/orders.json?auth=${action.token}`, action.orderData)
        yield put(actions.purchaseBurgerSuccess(response.data.name, action.orderData))
    } catch (error) {
        yield put(actions.purchaseBurgerFail(error))
    }
}

export function* fetchOrdersSaga(action){
    yield put(actions.fetchOrdersStart())
    try{
        const queryParams = "?auth=" + action.token + '&orderBy="userId"&equalTo="' + action.userId + '"'
        const response = yield Axios.get(`/orders.json${queryParams}`)
        const fetchedOrders = []
        for (let key in response.data) {
            fetchedOrders.push({
                ...response.data[key],
                id: key
            })
        }
        yield put(actions.fetchOrdersSuccess(fetchedOrders))
         
    } catch(err){
        yield put(actions.fetchOrdersFail(err))
    }
}