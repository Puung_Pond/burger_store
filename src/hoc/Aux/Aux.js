// Use for wrapping <div></div> component
// Instead of using with <div></div>

const aux = (props) => props.children

export default aux